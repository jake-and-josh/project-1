#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>



Randcard(int a){
    a = (rand() % 13) + 1;
    return a;
}
Randsuit(int b){
    b = rand() % 4;
    return b;
}



int main(void){
    srand (time(NULL));
   
    int numCard1;
    int Suit1;
    int numCard2;
    int Suit2;
    int userGuess;
    int answerP;
    int answerM;


    numCard1 = (rand() % 13) + 1;
    Suit1 = (rand() % 4);
    numCard2 = (rand() % 13) + 1;
    Suit2 = (rand() % 4);

    numCard1 = Randcard(1);
    Suit1 = Randsuit(1);
   

    printf("Welcome to the card game! Drawing a card...\n");

    if((numCard1 > 1) && (numCard1 < 11)){
        printf("Your first card is the %d of ", numCard1);
   
    }
    else if(numCard1 == 1){
        printf("Your first card is the Ace of ");
        }
    else if(numCard1 == 11){
        printf("Your first card is the Jack of ");
        }
    else if(numCard1 == 12){
        printf("Your first card is the Queen of ");
        }
    else if(numCard1 == 13){
        printf("Your first card is the King of ");
        }

    if(Suit1 == 0){
        printf("Diamonds");
    }

      if(Suit1 == 1){
        printf("Spades");
    }

      if(Suit1 == 2){
        printf("Clubs");
    }

      if(Suit1 == 3){
        printf("Hearts");
    }
    printf("!\n");

    printf("Guess a number: ");
    scanf("%d", &userGuess);
   
    if((numCard2 > 1) && (numCard2 < 11)){
        printf("Your second card is the %d of ", numCard2);
   
    }
    else if(numCard2 == 1){
        printf("Your second card is the Ace of ");
        }
    else if(numCard2 == 11){
        printf("Your second card is the Jack of ");
    }
    else if(numCard2 == 12){
        printf("Your second card is the Queen of ");
    }
    else if(numCard2 == 13){
        printf("Your second card is the King of ");
    }

    if(Suit2 == 0){
        printf("Diamonds");
    }

      if(Suit2 == 1){
        printf("Spades");
    }

      if(Suit2 == 2){
        printf("Clubs");
    }

      if(Suit2 == 3){
        printf("Hearts");
    }
    printf("!\n");

    if (numCard1 > 10){
        numCard1 = 10;
    }

    if (numCard2 > 10){
        numCard2 = 10;
    }

   

    answerP = numCard1 + numCard2;
    printf("%d\n", answerP);
    answerM = numCard1 - numCard2;
    printf("%d\n", answerM);
    if (numCard1 + numCard2 == userGuess){
        printf("You win! The sum of the two cards is %d.\n\n", answerP);
    }
        else if (numCard1 - numCard2 == userGuess){
            printf("You win! The difference of the two cards is %d.\n\n", abs(answerM));
        }
        else {
            printf("You lose. The correct guess is %d, or %d\n\n", answerP, abs(answerM));
           
        }


return 0;

}